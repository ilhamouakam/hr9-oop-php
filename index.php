<?php 
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    

    $sheep  = new animal("Shaun",4,"no");
    echo "Nama Hewan: " . $sheep ->name."<br>";
    echo "Jumlah Kaki: " . $sheep ->leg."<br>";
    echo "Darah Dingin: " . $sheep ->cold_blooded."<br><br>";

    
    $kodok  = new kodok("Buduk",4,"Yes");
    echo "Nama Hewan: " . $kodok ->name."<br>";
    echo "Jumlah Kaki: " . $kodok ->leg."<br>";
    echo "Darah Dingin: " . $kodok ->cold_blooded."<br>";
    echo "Jump: " . $kodok ->jump."<br><br>";


    $sungokong = new kera("Kera Sakti",2,"no");
    echo "Nama Hewan: " . $sungokong->name."<br>";
    echo "Jumlah Kaki: " . $sungokong->leg."<br>";
    echo "Darah Dingin: " . $sungokong->cold_blooded."<br>";
    echo "Yell: " . $sungokong->yell."<br>";

?>